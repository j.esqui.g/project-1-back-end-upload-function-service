const { Storage } = require("@google-cloud/storage");
const storage = new Storage();
const cors = require('cors')(); // have cause error

exports.upload = async (req, res) => 
{
cors (req,res, async () => {
const body = req.body;


    //file_size in bytes = len(base64_string) * 3 / 4 - base64_string.count('=')
    let biteFileSize = (body.content.length * 3/4) - (body.content.match(/=/g) || []).length;
    let mbFileSize = biteFileSize/1000000; // get size in mb
   // console.log(biteFileSize)
   // console.log(mbFileSize)

    if(mbFileSize < 10)
    {
        if(body.extension === "png" || body.extension === "jpg")
        {
            const bucket = storage.bucket('image-bucket-project1');
            const buffer = Buffer.from(body.content,'base64');
            const file = bucket.file(`${body.name}.${body.extension}`);
            await file.save(buffer)
            await file.makePublic();
            res.send({ photoLink: file.publicUrl() });
        }
        else
        {
            res.send("Wrong extensions")
        }
    }
    else
    {
        res.send("File too big")
    }    
})
}
